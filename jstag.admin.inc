<?php

/**
 * @file
 * admin page callbacks and form handlers for the jstag module
 */

/**
 * Page callback for jastag admin page (admin/settings/jstag).
 *
 * @return array
 *   the themed output for the admin overview page
 */
function jstag_admin_overview() {
  drupal_add_css(drupal_get_path('module', 'jstag') . '/css/jstag.css');
  drupal_add_js(drupal_get_path('module', 'jstag') . '/js/jstag.js');
  $output = drupal_get_form('jstag_overview_form');

  return $output;
}

/**
 * Theme function for the jstag admin overview table.
 *
 * @param array $form
 *   an associative array of arguments (should only contain $form array)
 *
 * @return array
 *   the admin overview form, themed as a draggable table of jstag
 */
function theme_jstag_overview_table($form) {
  global $base_url;
  $output = '';
  $regions = unserialize($form['regions']['#value']);

  if (empty($regions['header']) && empty($regions['page_top']) && empty($regions['page_bottom'])) {
    $link = l(t('Click here'), 'admin/settings/jstag/add');
    $output .= t('You have not configured any javascript snippets. !link to add one.', array('!link' => $link));
    return $output;
  }

  // Create a table for each region.
  foreach ($regions as $region => $snippets) {
    $rows = array();
    if (empty($snippets)) {
      // Nothing to do here...
      continue;
    }

    // Sort rows by weight before creating table.
    usort($snippets, '_jstag_weight_sort');

    // Build a row for each snippet.
    foreach ($snippets as $id => $snippet) {
      $actions = array();
      $enabled = ($snippet->status) ? t('Disable') : t('Enable');
      $actions = array(
        l($enabled, 'admin/settings/jstag/' . $snippet->jid . '/disable', array(
          'attributes' => array('class' => 'jstag-disable-link', 'rel' => $snippet->jid),
          'html' => TRUE,
          'fragment' => '',
        )),
        l(t('Configure'), 'admin/settings/jstag/' . $snippet->jid . '/edit'),
        l(t('Delete'), 'admin/settings/jstag/' . $snippet->jid . '/delete'),
      );

      $rows[] = array(
        'data' => array(
          array(
            'data' => check_plain($snippet->name),
            'class' => 'jstag-name',
          ),
          array(
            'data' => check_plain(truncate_utf8($snippet->code, 150, FALSE, '...')),
            'class' => 'jstag-code',
          ),
          array(
            'data' => implode(' | ', $actions)
          ),
          array(
            'data' => drupal_render($form['jstag'][$region]['weight_'.$snippet->jid])
          )
        ),
        // Make all rows draggable.
        'class' => ($snippet->status) ? 'draggable' : 'draggable jstag-disabled',
      );
    }

    switch ($region) {
      case 'header':
        $output .= '<h2>In &lt;HEAD&gt;</h2>';
        break;

      case 'page_top':
        $output .= '<h2>After &lt;BODY&gt;</h2>';
        break;

      case 'page_bottom':
        $output .= '<h2>Before &lt;/BODY&gt;</h2>';
        break;
    }

    $header = array(
      array('data' => t('Name')),
      array('data' => t('Code')),
      array('data' => t('Actions')),
      array('data' => t('Weight'))
    );

    $output .= theme('table', $header, $rows, array('id' => 'jstag-admin-table-' . $region));

    drupal_add_tabledrag('jstag-admin-table-' . $region, 'order', 'siblings', 'snippet-weight');
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Form for jstag overview table.
 *
 * @return array
 *   a renderable Form API array
 */
function jstag_overview_form(&$form_state, $format = NULL) {
  $regions = _jstag_all_by_region();

  $form = array(
    '#type' => 'form',
    '#theme' => 'jstag_overview_table',
    '#theme_wrappers' => array('form'),
  );

  $form['regions'] = array(
    '#type' => 'value',
    '#value' => serialize($regions),
  );

  foreach ($regions as $region => $snippets) {
    foreach ($snippets as $id => $snippet) {
      // Build a form element for each weight attribute.
      $form['jstag'][$region]['weight_' . $id] = array(
        '#type' => 'textfield',
        '#default_value' => $snippet->weight,
        '#attributes' => array('class' => 'snippet-weight', 'size' => 3),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form for adding codeblocks (admin/settings/jstag/add).
 *
 * @return array
 *   a renderable Form API array
 */
function jstag_add_form() {
  $form['jstag_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Enter a name to describe this javascript snippet'),
  );

  $form['jstag_code'] = array(
    '#type' => 'textarea',
    '#title' => t('Code'),
    '#description' => t('Paste your javascript snippet here'),
  );

  if (module_exists('token')) {
    $form['tokens'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('node'),
      '#global_types' => TRUE,
      '#click_insert' => TRUE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Snippet'),
  );

  return $form;
}

/**
 * Form for editing codeblocks (admin/settings/jstag/%/edit).
 *
 * @param (int) $delta
 *   the array index of the codeblock to edit
 *
 * @return array
 *   a renderable Form API array
 */
function jstag_edit_form(&$form_state, $delta) {
  _jstag_set_breadcrumb();
  $snippet = _jstag_read($delta);

  // Don't show a blank edit form.
  if (!$snippet) {
    drupal_not_found();
    exit();
  }

  $form['delta'] = array(
    '#type' => 'hidden',
    '#value' => $delta,
  );

  $form['jstag_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Enter a name to describe this code block'),
    '#default_value' => $snippet['name'],
  );

  $form['jstag_status'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Javascript Snippet'),
    '#description' => t("Javascript snippets are disabled by default, so you won't accidentally make tracking code live if you didn't intend to."),
    '#default_value' => $snippet['status'],
    '#options' => array(1 => t('Active'), 0 => t('Disabled')),
  );

  $form['jstag_code'] = array(
    '#type' => 'textarea',
    '#title' => t('Code'),
    '#description' => t('Enter your javascript snippet here'),
    '#default_value' => $snippet['code'],
  );

  if (module_exists('token')) {
    $form['tokens'] = array(
      '#value' => theme('token_tree', array('node'), TRUE, TRUE)
    );
  }

  $form['jstag_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility Settings'),
    '#collapsible' => TRUE,
  );

  $form['jstag_options']['jstag_region'] = array(
    '#type' => 'radios',
    '#title' => t('Render this code:'),
    '#options' => array(
      'header' => t('Inside &lt;HEAD&gt;'),
      //TODO Implement 'After <BODY>' snippets
      //'page_top' => t('After &lt;BODY&gt;'),
      'page_bottom' => t('Before &lt;/BODY&gt;'),
    ),
    '#default_value' => $snippet['region'],
  );

  $form['jstag_options']['jstag_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Invoke this javascript snippet on specific pages:'),
    '#options' => array(
      JSTAG_VISIBILITY_NOTLISTED => t('All pages except those listed'),
      JSTAG_VISIBILITY_LISTED => t('Only the listed pages'),
    ),
    '#default_value' => $snippet['visibility'],
  );

  $form['jstag_options']['jstag_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. &lt;front&gt; is the front page."),
    '#default_value' => $snippet['pages'],
  );

  $form['jstag_options']['jstag_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#options' => node_get_types('names'),
    '#default_value' => unserialize($snippet['content_types']),
    '#description' => t('Show this javascript snippet only on pages that display content of the given type(s). If you select no types, there will be no type-specific limitation.'),
  );

  $form['jstag_options']['jstag_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User Roles'),
    '#options' => user_roles(),
    '#description' => t('Show this javascript snippet only to users of a specific Drupal role. If you select no roles, this snippet will be shown for all roles.'),
  );
  if (isset($snippet['roles'])) {
    $form['jstag_options']['jstag_roles']['#default_value'] = unserialize($snippet['roles']);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Snippet'),
  );

  return $form;
}

/**
 * Form for deleting codeblocks (admin/settings/jstag/%/delete).
 *
 * @param (int) $delta
 *   the array index of the codeblock to edit
 *
 * @return array
 *   a renderable Form API array or 404
 */
function jstag_delete_form(&$form_state, $delta) {
  $snippet = _jstag_read($delta);

  // Don't show a blank delete form.
  if (!$snippet) {
    drupal_not_found();
    exit();
  }

  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);
  $form['snippet_name'] = array('#type' => 'hidden', '#values' => $snippet['name']);

  return confirm_form($form, t('Are you sure you want to delete the javascript snippet %name?', array('%name' => $snippet['name'])), 'admin/settings/jstag', '', t('Delete'), t('Cancel'));
}

/**
 * Page callback for AJAX enable/disable request on a codeblock.
 *
 * @param (int) $delta
 *   the primary key of the javascript snippet to enable/disable
 *
 * @return string
 *   a JSON object containing the status and label replacement
 */
function jstag_ajax_disable($delta) {
  $snippet = _jstag_read($delta);
  $status = ($snippet['status']) ? 0 : 1;
  db_query("UPDATE {jstag} SET status = %d WHERE jid = %d", $status, $delta);


  drupal_set_message(t('Javascript snippet weights have been updated.'));

  drupal_goto('admin/settings/jstag');

  $response = array(
    'status' => $status,
    'label' => ($status ? t('Disable') : t('Enable')),
  );
  //TODO Add ajax support
  //print drupal_json_encode($response);
  //exit();
}


/**
 * ### SUBMIT/VALIDATE FUNCTIONS ###
 */


/**
 * Validation function for jstag overview/weight table.
 */
function jstag_overview_form_validate($form, $form_state) {
  foreach ($form_state['values'] as $key => $value) {
    if (preg_match('/^weight_[0-9]+$/', $key)) {
      if (!is_numeric($value)) {
        form_set_error($key, t('Weight attribute must be a numeric value.'));
      }
    }
  }
}

/**
 * Submit function for jstag overview/weight table.
 */
function jstag_overview_form_submit($form, &$form_state) {

  foreach ($form_state['values'] as $key => $value) {
    if (preg_match('/^weight_[0-9]+$/', $key)) {
      $jid = str_replace('weight_', '', $key);
      db_query("UPDATE {jstag} SET weight = %d WHERE jid = %d", $value, $jid);
    }
  }
  drupal_set_message(t('Javascript snippet weights have been updated.'));
}

/**
 * Submit function for jstag add form.
 */
function jstag_add_form_submit($form, &$form_state) {
  db_query("INSERT INTO {jstag} (name, code, status, weight, visibility, pages, content_types, roles) VALUES ('%s', '%s', %d, %d, %d, '%s', '%s', '%s')",
    $form_state['values']['jstag_name'],
    $form_state['values']['jstag_code'],
    0,
    0,
    1,
    '',
    serialize(array()),
    serialize(array())
  );
  $jid = db_last_insert_id('jstag', 'jid');

  $form_state['redirect'] = 'admin/settings/jstag/' . $jid . '/edit';

  drupal_set_message(t('Created new javascript snippet "%name."', array('%name' => $form_state['values']['jstag_name'])));
}

/**
 * Submit function for jstag edit form.
 */
function jstag_edit_form_submit($form, &$form_state) {
  db_query("UPDATE {jstag} SET name = '%s', code = '%s', region = '%s', status = %d, visibility = %d, pages = '%s', content_types = '%s', roles = '%s' WHERE jid = %d",
    $form_state['values']['jstag_name'],
    $form_state['values']['jstag_code'],
    $form_state['values']['jstag_region'],
    $form_state['values']['jstag_status'],
    $form_state['values']['jstag_visibility'],
    $form_state['values']['jstag_pages'],
    serialize($form_state['values']['jstag_content_types']),
    serialize($form_state['values']['jstag_roles']),
    $form_state['values']['delta']);

  $form_state['redirect'] = 'admin/settings/jstag';
  drupal_set_message(t('Updated javascript snippet "%name."', array('%name' => $form_state['values']['jstag_name'])));
}

/**
 * Submit function for jstag delete form.
 */
function jstag_delete_form_submit($form, &$form_state) {
  db_query('DELETE FROM {jstag} WHERE jid = %d', $form_state['values']['delta']);

  $form_state['redirect'] = 'admin/settings/jstag';
  drupal_set_message(t('Deleted javascript snippet "%name."', array('%name' => $form_state['values']['snippet_name'])));
}
