<?php

/**
 * @file
 * main module file for jstag module
 */

/**
 * visibility constants (a la block module)
 */
define('JSTAG_VISIBILITY_NOTLISTED', 0);
define('JSTAG_VISIBILITY_LISTED', 1);

/**
 * Implements hook_init().
 */
function jstag_init() {
  $snippets = _jstag_enabled_by_region();

  $node = menu_get_object();

  // Render jstag in <HEAD> immediately.
  // Other jstag will be rendered by hook_page_alter().
  foreach ($snippets['header'] as $id => $snippet) {
    drupal_set_html_head($snippet->code);
  }
}

/**
 * This hook does not exist in Drupal 6. Function left here for documenting
 * unimplemented features
 *
 * TODO Remove this function after implementing 'After <BODY>' feature
 *
 * Implements hook_page_alter().
 */
function jstag_page_alter(&$page) {
  $snippets = _jstag_enabled_by_region();

  $node = menu_get_object();

  //TODO Add support for 'After <BODY>' snippets if possible
  // Render "After <BODY>" javascript snippet.
  foreach ($snippets['page_top'] as $snippet) {
    $page['page_top']['tracking_code'][$snippet->name] = array(
      '#markup' => token_replace($snippet->code, array('node' => $node)),
      '#weight' => $snippet->weight,
    );
  }
}

/**
 * Implements hook_footer().
 */
function jstag_footer($main) {
  $snippets = _jstag_enabled_by_region();
  $output = '';
  foreach ($snippets['page_bottom'] as $id => $snippet) {
    $output .= $snippet->code;
  }
  return $output;
}

/**
 * Implements hook_menu().
 */
function jstag_menu() {
  $items['admin/settings/jstag'] = array(
    'title' => 'Javascript Snippets',
    'description' => 'Create javascript snippets and associate them with paths',
    'page callback' => 'jstag_admin_overview',
    'access arguments' => array('administer jstag'),
    'file' => 'jstag.admin.inc',
  );

  $items['admin/settings/jstag/list'] = array(
    'title' => 'List',
    'page callback' => 'jstag_admin_overview',
    'access arguments' => array('administer jstag'),
    'file' => 'jstag.admin.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );

  $items['admin/settings/jstag/add'] = array(
    'title' => 'Add Javascript snippet',
    'description' => 'Create a javascript snippet',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jstag_add_form'),
    'access arguments' => array('administer jstag'),
    'file' => 'jstag.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/jstag/%/edit'] = array(
    'title' => 'Edit Javascript Snippet',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jstag_edit_form', 3),
    'access arguments' => array('administer jstag'),
    'file' => 'jstag.admin.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/jstag/%/delete'] = array(
    'title' => 'Delete Javascript Snippet',
    'description' => 'Delete an existing javascript snippet',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jstag_delete_form', 3),
    'access arguments' => array('administer jstag'),
    'file' => 'jstag.admin.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/jstag/%/disable'] = array(
    'title' => 'Disable/Enable Javascript Snippets',
    'page callback' => 'jstag_ajax_disable',
    'page arguments' => array(3),
    'access arguments' => array('administer jstag'),
    'file' => 'jstag.admin.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function jstag_perm() {
  return array('administer jstag');
}

/**
 * Implements hook_theme().
 */
function jstag_theme() {
  return array(
    'jstag_overview_table' => array(
      'render element' => 'form',
      'file' => 'jstag.admin.inc',
    ),
  );
}


/**
 * ### API FUNCTIONS ###
 */


/**
 * Read a single tracking code snippet from the database.
 *
 * @param (int) $delta
 *   the primary key of the snippet to load
 *
 * @return array/FALSE
 *   Array: a tracking code record object
 *   FALSE: snippet doesn't exist in database
 */
function _jstag_read($delta) {
  $query = "SELECT * FROM {jstag} j WHERE jid = %d";
  $result = db_query($query, $delta);
  $snippet = db_fetch_array($result);
  return (isset($snippet['jid'])) ? $snippet : FALSE;
}

/**
 * Process active tracking code snippets into an array.
 *
 * @return array
 *   an array of active tracking code objects, organized by region
 */
function _jstag_enabled_by_region() {
  static $snippets;

  if (isset($snippets)) {
    return $snippets;
  }

  $node = menu_get_object();

  $snippets = array(
    'header' => array(),
    'page_top' => array(),
    'page_bottom' => array(),
  );

  $query = "SELECT * FROM {jstag} j WHERE j.status = 1";
  $result = db_query($query);

  // Check page-level visibility.
  while ($snippet = db_fetch_object($result)) {
    $content_types = unserialize($snippet->content_types);
    $roles = unserialize($snippet->roles);
    $selected_types = array();
    $selected_roles = array();

    // Build list of content types.
    foreach ($content_types as $type => $status) {
      if ($status) {
        $selected_types[] = $type;
      }
    }

    // Build list of roles.
    foreach ($roles as $role_id => $status) {
      if ($status) {
        $selected_roles[] = $role_id;
      }
    }

    // Need to match clean URLs as well as Drupal paths.
    $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
    $page_match = drupal_match_path($path, $snippet->pages);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $snippet->pages);
    }

    switch ($snippet->visibility) {
      case JSTAG_VISIBILITY_NOTLISTED:
        if (!$page_match) {
          $snippets[$snippet->region][$snippet->jid] = $snippet;
        }
        break;

      case JSTAG_VISIBILITY_LISTED:
        if ($page_match) {
          $snippets[$snippet->region][$snippet->jid] = $snippet;
        }
        break;
    }

    // Unset snippet if not in selected content types.
    if (!empty($selected_types)) {
      if (!$node || !in_array($node->type, $selected_types)) {
        unset($snippets[$snippet->region][$snippet->jid]);
      }
    }

    // Unset snippet if not in selected user roles.
    if (!empty($selected_roles)) {
      global $user;
      $show = FALSE;
      foreach ($user->roles as $role_id => $role) {
        if (in_array($role_id, $selected_roles)) {
          $show = TRUE;
          break;
        }
      }

      if (!$show) {
        unset($snippets[$snippet->region][$snippet->jid]);
      }
    }
  }

  foreach ($snippets as $region => &$snippets_by_region) {
    // Sort rows by weight
    usort($snippets_by_region, '_jstag_weight_sort');
    if(module_exists('token')) {
      foreach ($snippets_by_region as &$snippet) {
        $snippet->code = token_replace($snippet->code, 'node', $node);
      }
    }
  }

  return $snippets;
}

/**
 * Process all tracking code snippets into an array.
 *
 * @return array
 *   an array of all tracking code objects, organized by region
 */
function _jstag_all_by_region() {
  $snippets = array(
    'header' => array(),
    'page_top' => array(),
    'page_bottom' => array(),
  );

  $query = "SELECT * FROM {jstag}";
  $result = db_query($query);

  while ($snippet = db_fetch_object($result)) {
    $snippets[$snippet->region][$snippet->jid] = $snippet;
  }

  return $snippets;
}

/**
 * Sets the Drupal breadcrumb on admin pages.
 */
function _jstag_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Settings'), 'admin/settings'),
    l(t('Javascript Snippets'), 'admin/settings/jstag'),
  );

  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Custom sort callback for sorting codeblocks by weight.
 *
 * used by usort() to sort objects by their "weight" attribute
 *
 * @param stdClass $a
 *   first comparison object
 * @param stdClass $b
 *   second comparison object
 *
 * @return int
 *   0 = objects have equal weight
 *   1 = object $a has greater weight
 *  -1 = object $b has greater weight
 */
function _jstag_weight_sort($a, $b) {
  if ($a->weight == $b->weight) {
    return 0;
  }
  return ($a->weight > $b->weight) ? 1 : -1;
}
