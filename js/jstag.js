(function ($) {

  // enable/disable codeblocks using AJAX call
  Drupal.behaviors.jstag = {
    attach: function (context, settings) {
      $('.jstag-disable-link', context).click(function () {
        anchor = $(this);
        delta = anchor.attr('rel');
        loading = $('<div class="jstag-loading"></div>');

        anchor.after(loading);
        ajaxResponse = $.getJSON(Drupal.settings.basePath + 'admin/settings/jstag/' + delta + '/disable', function(response) {
          if (response.status) {
            anchor.parents('tr').removeClass('jstag-disabled');
          } else {
            anchor.parents('tr').addClass('jstag-disabled');
          }
          anchor.html(response.label);
          loading.remove();
        });

        return false;
      });
    }
  };

}(jQuery));
